#ifndef CutsodSkimmer_H
#define CutsodSkimmer_H

#include "EventBase.h"
#include "odSkimmerEvent.h"

class CutsodSkimmer  {

public:
  CutsodSkimmer(odSkimmerEvent* m_evt);
  ~CutsodSkimmer();
  bool odSkimmerCutsOK();
  bool odSkimmerNumPulsesGT10();

private:
  
  odSkimmerEvent* m_evt;

};

#endif
