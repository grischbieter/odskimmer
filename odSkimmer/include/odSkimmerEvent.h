#ifndef odSkimmerEVENT_H
#define odSkimmerEVENT_H

#include "EventBase.h"

#include <vector>
#include <string>

#include "rqlibProjectHeaders.h"


class odSkimmerEvent : public EventBase {

 public:

  odSkimmerEvent(EventBase* eventBase);
  virtual ~odSkimmerEvent();


  //  GET STRUCTURE AND VARIABLE TYPE FROM modules/rqlib/
  TTreeReaderValue<string> rawFileName;
  TTreeReaderValue<unsigned long> eventID;
  TTreeReaderValue<unsigned long> runID;


  TTreeReaderValue<int> nODpulses;
  TTreeReaderValue<vector<float>> odPulseArea;
  TTreeReaderValue<vector<int>> coincidence;

  TTreeReaderValue<int> nSingleScatters;
  TTreeReaderValue<int> nMultipleScatters;

  TTreeReaderValue<float> ss_S1;
  TTreeReaderValue<float> ms_S1;

  TTreeReaderValue<int> s1ID_ss;
  TTreeReaderValue<int> s1ID_ms;

 private:

  


};


#endif // odSkimmerEVENT_H
