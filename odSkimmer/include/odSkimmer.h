#ifndef odSkimmer_H
#define odSkimmer_H

#include "Analysis.h"

#include "EventBase.h"
#include "CutsodSkimmer.h"

#include <TTreeReader.h>
#include <string>

#include "odSkimmerEvent.h"

class odSkimmer: public Analysis {

public:
  odSkimmer(); 
  ~odSkimmer();

  void Initialize();
  void Execute();
  void Finalize();

  

protected:
  CutsodSkimmer* m_cutsodSkimmer;
  ConfigSvc* m_conf;
  odSkimmerEvent* m_evt;
};

#endif
