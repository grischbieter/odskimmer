#include "EventBase.h"
#include "Analysis.h"
#include "HistSvc.h"
#include "Logger.h"

#include "CutsBase.h"
#include "ConfigSvc.h"
#include "odSkimmer.h"
#include "SkimSvc.h"

// Constructor
odSkimmer::odSkimmer()
  : Analysis(),
   m_evt(new odSkimmerEvent(m_event))
{

  m_event->Initialize();

  // Setup logging
  logging::set_program_name("odSkimmer Analysis");
  // Logging level: error = 1, warning = 2, info = 3, debug = 4, verbose = 5
  
  
  //create a config instance, this can be used to call the config variables. 
  m_conf = ConfigSvc::Instance();

  m_skim = new SkimSvc(); 
}

// Destructor
odSkimmer::~odSkimmer() {
  delete m_skim;
}

// Called before event loop
void odSkimmer::Initialize() {
  INFO("Initializing odSkimmer Analysis");
  m_skim->InitializeSkim(m_event);
}
int nEvents = 0, nAboveODthreshold = 0, nOD = 0;
// Called once per event
void odSkimmer::Execute() {


  nEvents++;
  bool oneInOD = false;  
  bool oneAboveThreshold = false;
  for (int i = 0; i < (*m_evt->nODpulses); i++) {
     if ( (*m_evt->coincidence)[i] > 4 ) {
       oneInOD = true;
       if ( (*m_evt->odPulseArea)[i] >= 100. ){  //set the threshold here 
           nAboveODthreshold++;
           oneAboveThreshold = true;
           break;
       }
     }
  }
  if ( oneInOD ) nOD++;
  if ( oneAboveThreshold ) {   //if the OD meets the pulse area requirements, check if SS or MS
    if ((*m_evt->nSingleScatters) == 1 ) {
       if (*m_evt->ss_S1 < 1000. ){   //relevance cut
          m_skim->SkimEvent(m_event);
       }
    }
    if ( (*m_evt->nMultipleScatters) == 1 ){
       if (*m_evt->ms_S1 < 1000.){   //relevance cut
          m_skim->SkimEvent(m_event);
       }
    }
  } 


}

// Called after event loop
void odSkimmer::Finalize() {
   cout << "Done!!!   nEvents: " << nEvents << "  nOD: " << nOD << "   " << " nAboveODthreshold: " << nAboveODthreshold << endl; 
}

