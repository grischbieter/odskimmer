#include "odSkimmerEvent.h"

odSkimmerEvent::odSkimmerEvent(EventBase* base):

      
  nODpulses(base->m_reader, "pulsesODHG.nPulses"),
  odPulseArea(base->m_reader, "pulsesODHG.pulseArea_phd"),
  coincidence(base->m_reader, "pulsesODHG.coincidence"),

  nSingleScatters(base->m_reader, "ss.nSingleScatters"),
  nMultipleScatters(base->m_reader, "ms.nMultipleScatters"),

  ss_S1(base->m_reader, "ss.s1Area_phd"),
  ms_S1(base->m_reader, "ms.s1Area_phd"),
  
  s1ID_ss(base->m_reader, "ss.s1PulseID"),
  s1ID_ms(base->m_reader, "ms.s1PulseID"),
  
  rawFileName(base->m_reader, "eventHeader.rawFileName"),
  runID(base->m_reader, "eventHeader.runID"),
  eventID(base->m_reader, "eventHeader.eventID")


{
}

odSkimmerEvent::~odSkimmerEvent()
{}

